# analytical-box

A containerised environment for analytical activities

## Current version

The current version of a-box is v0.1.6, it's source files can be found in [unbound_box](unbound_box)

## History

Analytical box started life as a containerised version of [binder](https://github.com/malariagen/binder). This version of a-box can be found in the [binder_based](binder_based) directory. Binder contains a lot of unneeded packages so Issue #3 was created to remove some of the unwanted packages. This led to [skinny_binder](skinny_binder), during the creation of skinny_binder to became apparent that Conda was not working well in this environment. Conda has bugs that prevented packages being installed or updated, is heavily coupled with the python version, and could not handle the dependancies for the number of packages a-box has. Issue #6 was created to remove Conda which takes us to the current latest version [unbound_box](unbound_box).

## Usage

Information on usage can be found in [unbound_box](unbound_box)
