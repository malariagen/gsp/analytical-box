# Binder_based

This version of analytical-box is based on Debian and uses the Conda environment created in [binder](https://github.com/malariagen/binder).

- The version of Conda comes from the docker container continuumio/miniconda3  
- The version of binder is 2.4.3

In addition to the packages provided by binder the following are installed

| package | channel |
|---------|---------|
|  r-irkernel | r |
|r-ape | r |
| geopandas | conda-forge |

## How to access

For docker download the [Dockerfile](Dockerfile)  
For singularity download [binder2.4.3.img](binder2.4.3.img)

# jupyter notebooks

## How to run on docker

```bash
docker run -i --rm -p 8888:8888 binder jupyter notebook --ip="*" --allow-root --no-browser
```

## How to run on singularity

```bash
singularity exec binder2.4.3.img jupyter notebook --ip="*" --no-browser
```

## Setting up & running on Gen3

The singularity image can be found here
```bash
/nfs/team112/software/analytical-box/binder2.4.3.img
```

Create an alias to run binder with lustre and nfs mounted  
```bash
alias analytical-box="/software/singularity-v3.5.3/bin/singularity exec --bind /lustre:/lustre,/nfs:/nfs /nfs/team112/software/analytical-box/binder2.4.3.img"
```

Start a notebook  
```bash
analytical-box jupyter notebook --ip="*" --no-browser
```
