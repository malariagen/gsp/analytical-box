echo "[default]" >> config.s3cfg
echo "encrypt = False" >> config.s3cfg
echo "host_base = cog.sanger.ac.uk" >> config.s3cfg
echo "host_bucket = %(bucket)s.cog.sanger.ac.uk" >> config.s3cfg
echo "progress_meter = True" >> config.s3cfg
echo "use_https = True" >> config.s3cfg
echo "access_key = $S3_VALUE" >> config.s3cfg
echo "secret_key = $S3_OTHER" >> config.s3cfg
