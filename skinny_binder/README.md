# Skinny binder

- This version of analytical-box is based on Debian and uses the Conda environment `environment-v.yml`
- The version of Conda is Miniconda3-4.6.14-Linux-x86_6, it does not use the latest due to this issue https://github.com/conda/conda/issues/9367

Skinny binder provides the following tools

| Package | Version |
|---------|---------|
| bcftools | 1.8 |
| biopython | 1.76 |
| blosc | 1.18.1 |
| bokeh | 2.0.1 |
| boto3 | 1.13.3 |
| botocore | 1.16.3 |
| bzip2 | 1.0.8 |
| cartopy | 0.17.0 |
| curl | 7.69.1 |
| cython | 0.29.17 |
| dask | 2.15.0 |
| dask-core | 2.15.0 |
| dask-glm | 0.2.0 |
| dask-kubernetes | 0.10.1 |
| dask-ml | 1.2.0 |
| gcsfs | 0.6.1 |
| h5py | 2.10.0 |
| hmmlearn | 0.2.3 |
| htslib | 1.10.2 |
| jupyter | 1.0.0 |
| jupyter_client | 6.1.3 |
| jupyter_console | 6.1.0 |
| jupyter_core | 4.6.3 |
| jupyterhub | 1.1.0 |
| jupyterlab | 2.1.2 |
| jupyterlab_launcher | 0.13.1 |
| jupyterlab_server | 1.1.1 |
| matplotlib | 3.2.1 |
| matplotlib-base | 3.2.1 |
| matplotlib-venn | 0.11.5 |
| msprime | 0.7.4 |
| mysql-connector-c | 6.1.11 |
| mysqlclient | 1.4.6 |
| nbconvert | 5.6.1 |
| nbformat | 5.0.6 |
| numcodecs | 0.6.4 |
| numpy | 1.18.4 |
| pandas | 1.0.3 |
| petl | 1.3.0 |
| pip | 20.1 |
| pyfaidx | 0.5.8 |
| pyfasta | 0.5.2 |
| python | 3.8.2 |
| pyvcf | 0.6.8 |
| readline | 8.0 |
| s3fs | 0.4.2 |
| samtools | 1.10 |
| scikit-allel | 1.2.1 |
| scikit-image | 0.16.2 |
| scikit-learn | 0.22.2.post1 |
| scipy | 1.4.1 |
| seaborn | 0.10.1 |
| setuptools | 46.1.3 |
| shapely | 1.7.0 |
| umap-learn | 0.4.2 |
| xarray | 0.15.1 |
| yaml | 0.2.4 |
| zarr | 2.4.0 |
| zlib | 1.2.11 |


## How to access

For docker download the [Dockerfile](Dockerfile)  
For singularity go to `/nfs/team112/software/analytical-box/skinny_v0.1.5.img`

# jupyter notebooks

## How to run on docker

```bash
docker run -i --rm -p 8888:8888 binder jupyter notebook --ip="*" --allow-root --no-browser
```

## How to run on singularity

```bash
singularity exec skinny_v0.1.5.img jupyter notebook --ip="*" --no-browser
```

## Setting up & running on Gen3

The singularity image can be found here
```bash
/nfs/team112/software/analytical-box/skinny_v0.1.5.img
```

Create an alias to run binder with lustre and nfs mounted  
```bash
alias analytical-box="/software/singularity-v3.5.3/bin/singularity exec --bind /lustre:/lustre,/nfs:/nfs /nfs/team112/software/analytical-box/skinny_v0.1.5.img"
```

Start a notebook  
```bash
analytical-box jupyter notebook --ip="*" --no-browser
```
