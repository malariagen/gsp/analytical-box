#!/usr/bin/env bash

export SINGULARITYENV_PREPEND_PATH=/usr/local/lsf/10.1/linux3.10-glibc2.17-x86_64/etc:/usr/local/lsf/10.1/linux3.10-glibc2.17-x86_64/bin

/software/singularity-v3.5.3/bin/singularity exec --bind /lustre:/lustre,/nfs:/nfs,/usr/local/lsf:/usr/local/lsf /nfs/team112/software/analytical-box/binder2.4.3.img $@
