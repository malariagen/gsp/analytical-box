## analytical-box v0.1.9 - Code name unbound-box

A Conda free version of analytical-box.

## How to access

For docker download the [Dockerfile](Dockerfile)  
For singularity go to `/nfs/team112/software/analytical-box/a-box_v0.1.9.sif`

You can also download a-box [here](https://abox.cog.sanger.ac.uk/index.html)

# jupyter notebooks

## How to run on docker

```bash
docker run -i --rm -p 8888:8888 unbound jupyter notebook --ip="*" --allow-root --no-browser
```

## How to run on singularity

```bash
singularity exec a-box_v0.1.9.sif jupyter notebook --ip="*" --no-browser
```

## Setting up & running on Gen3

The singularity image can be found here
```bash
/nfs/team112/software/analytical-box/a-box_v0.1.9.sif
```

Create an alias to run binder with lustre and nfs mounted  
```bash
alias analytical-box="/software/singularity-v3.5.3/bin/singularity exec --bind /lustre:/lustre,/nfs:/nfs /nfs/team112/software/analytical-box/a-box_v0.1.9.sif"
```

Start a notebook  
```bash
analytical-box jupyter notebook --ip="*" --no-browser
```

### Installed packages

Pip packages

| Package | Version |
|---------|---------|
| aiohttp | 3.7.2 |
| alabaster | 0.7.12 |
| alembic | 1.4.3 |
| anhima | 0.11.2 |
| argon2-cffi | 20.1.0 |
| asciitree | 0.3.3 |
| async-generator | 1.10 |
| async-timeout | 3.0.1 |
| attrs | 20.2.0 |
| Babel | 2.8.0 |
| backcall | 0.2.0 |
| bcrypt | 3.2.0 |
| biopython | 1.76 |
| bleach | 3.2.1 |
| blosc | 1.9.1 |
| bokeh | 2.0.2 |
| boto | 2.49.0 |
| boto3 | 1.13.9 |
| botocore | 1.16.9 |
| CacheControl | 0.12.6 |
| cachetools | 4.1.1 |
| Cartopy | 0.18.0 |
| certifi | 2020.6.20 |
| certipy | 0.1.3 |
| cffi | 1.14.3 |
| chardet | 3.0.4 |
| click | 7.1.2 |
| cloudpickle | 1.6.0 |
| cryptography | 3.2.1 |
| cycler | 0.10.0 |
| Cython | 0.29.21 |
| dask | 2.16.0 |
| dask-glm | 0.2.0 |
| dask-jobqueue | 0.7.1 |
| dask-kubernetes | 0.10.1 |
| dask-ml | 1.4.0 |
| decorator | 4.4.2 |
| defusedxml | 0.6.0 |
| dill | 0.3.2 |
| distributed | 2.30.0 |
| docutils | 0.15.2 |
| entrypoints | 0.3 |
| fasteners | 0.15 |
| fastlmm | 0.4.8 |
| fsspec | 0.8.4 |
| gcsfs | 0.6.2 |
| google-auth | 1.22.1 |
| google-auth-oauthlib | 0.4.2 |
| googlemaps | 4.4.0 |
| h5py | 2.10.0 |
| hdmedians | 0.14.1 |
| HeapDict | 1.0.1 |
| hmmlearn | 0.2.3 |
| idna | 2.10 |
| imageio | 2.9.0 |
| imagesize | 1.2.0 |
| ipykernel | 5.3.4 |
| ipython | 7.18.1 |
| ipython-genutils | 0.2.0 |
| ipywidgets | 7.5.1 |
| jedi | 0.17.2 |
| Jinja2 | 2.11.2 |
| jmespath | 0.10.0 |
| joblib | 0.17.0 |
| json5 | 0.9.5 |
| jsonschema | 3.2.0 |
| jupyter | 1.0.0 |
| jupyter-client | 6.1.3 |
| jupyter-console | 6.1.0 |
| jupyter-core | 4.6.3 |
| jupyter-telemetry | 0.1.0 |
| jupyterhub | 1.1.0 |
| jupyterlab | 2.1.2 |
| jupyterlab-launcher | 0.13.1 |
| jupyterlab-server | 1.1.1 |
| kiwisolver | 1.3.0 |
| kubernetes | 12.0.0 |
| kubernetes-asyncio | 12.0.1 |
| llvmlite | 0.34.0 |
| lockfile | 0.12.2 |
| Mako | 1.1.3 |
| MarkupSafe | 1.1.1 |
| matplotlib | 3.2.1 |
| matplotlib-venn | 0.11.5 |
| mistune | 0.8.4 |
| monotonic | 1.5 |
| msgpack | 1.0.0 |
| msprime | 0.7.4 |
| multidict | 5.0.0 |
| multipledispatch | 0.6.0 |
| mysql-connector | 2.2.9 |
| mysqlclient | 1.4.6 |
| natsort | 7.0.1 |
| nbconvert | 5.6.1 |
| nbformat | 5.0.6 |
| networkx | 2.5 |
| notebook | 6.1.4 |
| numba | 0.51.2 |
| numcodecs | 0.6.4 |
| numpy | 1.18.4 |
| numpydoc | 0.9.2 |
| oauthlib | 3.1.0 |
| packaging | 20.4 |
| pamela | 1.0.0 |
| pandas | 1.0.3 |
| pandocfilters | 1.4.3 |
| paramiko | 2.7.2 |
| parso | 0.7.1 |
| pathtools | 0.1.2 |
| patsy | 0.5.1 |
| petl | 1.3.0 |
| petlx | 1.0.3 |
| pexpect | 4.8.0 |
| Pgenlib | 0.73 |
| pickleshare | 0.7.5 |
| Pillow | 8.0.1 |
| pip | 20.1 |
| prometheus-client | 0.8.0 |
| prompt-toolkit | 3.0.8 |
| psutil | 5.7.3 |
| ptyprocess | 0.6.0 |
| pyasn1 | 0.4.8 |
| pyasn1-modules | 0.2.8 |
| pybedtools | 0.8.1 |
| pycparser | 2.20 |
| pycryptodomex | 3.9.8 |
| pyerf | 1.0.1 |
| pyfaidx | 0.5.8 |
| pyfasta | 0.5.2 |
| Pygments | 2.7.2 |
| PyNaCl | 1.4.0 |
| pyOpenSSL | 19.1.0 |
| pyparsing | 2.4.7 |
| pyrsistent | 0.17.3 |
| pysam | 0.15.4 |
| pysamstats | 1.1.2 |
| pyshp | 2.1.2 |
| pysnptools | 0.4.11 |
| python-dateutil | 2.8.1 |
| python-editor | 1.0.4 |
| python-json-logger | 2.0.1 |
| pytz | 2020.1 |
| PyVCF | 0.6.8 |
| PyWavelets | 1.1.1 |
| PyYAML | 5.3.1 |
| pyzipper | 0.3.3 |
| pyzmq | 19.0.2 |
| qtconsole | 4.7.7 |
| QtPy | 1.9.0 |
| readline | 6.2.4.1 |
| requests | 2.24.0 |
| requests-oauthlib | 1.3.0 |
| rsa | 4.6 |
| ruamel.yaml | 0.16.12 |
| ruamel.yaml.clib | 0.2.2 |
| s3fs | 0.4.2 |
| s3transfer | 0.3.3 |
| scikit-allel | 1.2.1 |
| scikit-bio | 0.5.6 |
| scikit-image | 0.17.2 |
| scikit-learn | 0.23.0 |
| scikits.bootstrap | 1.0.1 |
| scipy | 1.4.1 |
| seaborn | 0.10.1 |
| Send2Trash | 1.5.0 |
| setuptools | 46.3.0 |
| Shapely | 1.7.0 |
| six | 1.15.0 |
| snowballstemmer | 2.0.0 |
| sortedcontainers | 2.2.2 |
| Sphinx | 3.2.1 |
| sphinxcontrib-applehelp | 1.0.2 |
| sphinxcontrib-devhelp | 1.0.2 |
| sphinxcontrib-htmlhelp | 1.0.3 |
| sphinxcontrib-jsmath | 1.0.1 |
| sphinxcontrib-qthelp | 1.0.3 |
| sphinxcontrib-serializinghtml 1.1.4 |
| SQLAlchemy | 1.3.20 |
| statsmodels | 0.12.1 |
| svgwrite | 1.4 |
| tbb | 2020.3.254 |
| tblib | 1.7.0 |
| terminado | 0.9.1 |
| testpath | 0.4.4 |
| threadpoolctl | 2.1.0 |
| tifffile | 2020.10.1 |
| toolz | 0.11.1 |
| tornado | 6.0.4 |
| traitlets | 5.0.5 |
| tskit | 0.3.2 |
| typing-extensions | 3.7.4.3 |
| umap-learn | 0.4.2 |
| urllib3 | 1.25.11 |
| wcwidth | 0.2.5 |
| webencodings | 0.5.1 |
| websocket-client | 0.57.0 |
| wheel | 0.34.2 |
| widgetsnbextension | 3.5.1 |
| xarray | 0.15.1 |
| xlrd | 1.2.0 |
| yarl | 1.6.2 |
| zarr | 2.4.0 |
| zict | 2.0.0 |

R packages - Installed via R

| Package | Version |
|---------|---------|
| repr    |         |
| IRdisplay |       |
| IRkernel |        |

R package - Installed via APT

| Package | Version |
|---------|---------|
| r-base  |         |
| r-recommended |   |
| r-cran-ape |      |

APT packages

| Package  | Version |
|----------|---------|
| curl |  |
| rsync |  |
| zip |  |
| unzip |  |
| cython |  |
| bzip2 |  |
| libgeos-3.7.1 |  |
| libyaml-0-2 |  |
| zlib1g |  |
| bcftools |  |
| samtools |  |
| openjdk-11-jdk |  |
| libsqlite3-0 |  |
| libtiff5 |  |
| libcurl4 |  |
| libcurl3-gnutls |  |
| wget |  |
| ca-certificates |  |
| bwa | |
| bedtools | |

Manually installed packages

| Package  | Version |
|----------|---------|
| Proj     | 6.3.2 |
| Plink    | v2.00a2.3 |
| pgenlib  | v2.00a2.3 |

### Packages with possible issues

The following packages reported warnings during the installation process

- anhima
- pysnptools
- fastlmm
