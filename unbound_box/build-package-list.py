
import os
import sys
import subprocess
import re

def search_for_package(p):

 result = subprocess.run(" ".join(["pip","search","\"{}\"".format(p)]),stderr=subprocess.PIPE,stdout=subprocess.PIPE,shell=True)
 if result.returncode != 0:
  return(False,result.stderr.decode("UTF-8"))
 else:
  output = result.stdout.decode("UTF-8")
  lines = output.splitlines()
  options = []
  for l in lines:
   option = l.split(" - ")[0].strip()
   if " " in option:
    obits = option.split(" ")
    if len(obits[1]) > 2:
     obits[1] = obits[1][1:-1]
    if obits[0] == p:
     options.append(tuple(obits))
   else:
    print("strange option found {}".format(option))
  return(len(options) > 0,options)

#-------------------------------------------------------------------------------
packages = ["bcftools","biopython","blosc","bokeh","boto3","botocore","bzip2",
            "cartopy","curl","cython","dask","dask-core","dask-glm","dask-kubernetes",
            "dask-ml","gcsfs","h5py","hmmlearn","htslib","jupyter","jupyter_client","jupyter_console",
            "jupyter_core","jupyterhub","jupyterlab","jupyterlab_launcher","jupyterlab_server","matplotlib",
            "matplotlib-base","matplotlib-venn","msprime","mysql-connector-c","mysqlclient","nbconvert","nbformat",
            "numcodecs","numpy","pandas","petl","pip","pyfaidx","pyfasta","pysam","pysamstats","python","pyvcf",
            "readline","s3fs","samtools","scikit-allel","scikit-image","scikit-learn","scipy","seaborn","setuptools",
            "shapely","umap-learn","xarray","yaml","zarr","zlib","anhima","boto","pathtools","petlx","scikits-bootstrap"]

target = "../skinny_binder/environment-v.yml"

vpackages = []

with open(target,"r") as env:
 for line in env:
  p = line.strip()[2:]
  if "==" in p:
   bits = p.split("==")
  else:
   bits = p.split("=")
  if bits[0] in packages:
   vpackages.append(bits)

missing     = []
pipPackages = []
print("\nFound {} / {}".format(len(vpackages),len(packages)))

if len(vpackages) < len(packages):
 print("\n\nWARNING! PACKAGES MISSING!!!")
 vpack = [p[0] for p in vpackages]
 for p in packages:
  if p not in vpack:
   print("{}".format(p))
   missing.append((p,""))
 print("\n\n")

findPackages = False
if findPackages:
 print("Searching PIP")
 for p in vpackages:
  search = search_for_package(p[0])
  if search[0]:
   pipPackages.append({"package":p[0],"version":p[1],"options":search[1]})
  else:
   missing.append(p)

 with open("pip.tsv","w") as pipreport:
  pipreport.write("Pip packages {}".format(len(pipPackages)))
  pipreport.write("Package\tVersion\tAvailable\n")

  for p in pipPackages:
   for o in p["options"]:
    pipreport.write("{}\t{}\t{}\t{}\n".format(p["package"],p["version"],o[0],o[1]))

  pipreport.write("\n\nMissing {}\n".format(len(missing)))
  for m in missing:
   pipreport.write("{}\t{}\n".format(m[0],m[1]))



printTable = False
if printTable:
 print("| Package | Version |")
 print("|---------|---------|")
 colformat = "| {} | {} |"
 for p in vpackages:
  print(colformat.format(p[0],p[1]))
