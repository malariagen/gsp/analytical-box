import os
import sys
import subprocess
import re

def load_list(f):
 packages = []
 spaceSplit = re.compile(" +")
 with open(f,"r") as fpack:
  for line in fpack:
   if "==" in line:
    packages.append(tuple(line.split("==")))
   else:
    packages.append(tuple(spaceSplit.split(line)))
 return packages

listA = load_list(sys.argv[1])
listB = load_list(sys.argv[2])

foundCount = 0
for a in listA:
 found = False
 for b in listB:
  if a[0] == b[0]:
   print("{} found".format(a[0]))
   found = True
   foundCount += 1
   if a[1] != b[1]:
    print("Version mismatch! {} vs {}".format(a[1],b[1]))
   break
 if not found:
  print("{} NOT FOUND!!!".format(a[0]))

print("found {} out of {}".format(foundCount,len(listA)))
